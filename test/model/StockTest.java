package model;



import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class StockTest {
	
	private Stock stock;
	private RawMaterialType prof_1;
	private RawMaterialType prof_2;
	
	private RawMaterial raw_1;
	private RawMaterial raw_2;

	@Before
	public void setUp() throws Exception {
		stock = Stock.GetInstace();
		prof_1 = new RawMaterialType("TEST 40x20x2 Zártszelvény", 1);
		prof_2 = new RawMaterialType("TEST 20x20x2 Zártszelvény", 1);
		
		raw_1 = new RawMaterial(prof_1, new Material("vas", 1.0), 200);
		raw_2 = new RawMaterial(prof_1, new Material("vas", 1.0), 500);
	} 

	@Test
	public void testProfileAddition(){
		stock.AddProfile(prof_1);
		Assert.assertEquals(stock.GetAllProfile().contains(prof_1), true);
	}
	
	@Test
	public void testRawAddition(){
		stock.AddProfile(prof_1);
		stock.AddRaw(prof_1, raw_1);
		Assert.assertEquals(stock.GetProfileRaws(prof_1).contains(raw_1), true);
	}
	
	@Test
	public void testCombinedLengths(){
		stock.AddProfile(prof_1);
		stock.AddRaw(prof_1, raw_1);
		stock.AddRaw(prof_1, raw_2);
		Assert.assertEquals(stock.GetAllProfile().get(stock.GetAllProfile().indexOf(prof_1)).getAllLengths(), 700, 0);
	}
	
	@Test
	public void testRemoveProfileWithoutRaws(){
		stock.AddProfile(prof_1);
		stock.RemoveProfile(prof_1);
		Assert.assertEquals(stock.GetAllProfile().contains(prof_1), false);
	}
	
	@Test
	public void testEditProfile(){
		stock.AddProfile(prof_1);
		stock.EditProfile(prof_1, prof_2);
		Assert.assertEquals(stock.GetAllProfile().get(stock.GetAllProfile().indexOf(prof_1)).getId(), prof_1.getId());
		Assert.assertEquals(stock.GetAllProfile().get(stock.GetAllProfile().indexOf(prof_1)).getType(), prof_2.getType());
	}
	
	@Test
	public void testEditRaw(){
		stock.AddProfile(prof_1);
		stock.AddRaw(prof_1, raw_1);
		stock.AddRaw(prof_1, raw_2);
		stock.EditRaw(prof_1, raw_1, 300.0);
		
		Assert.assertEquals(stock.GetProfileRaws(prof_1).get(stock.GetProfileRaws(prof_1).indexOf(raw_1)).getLength(), 300, 0);
		Assert.assertEquals(stock.GetAllProfile().get(stock.GetAllProfile().indexOf(prof_1)).getAllLengths(), 800, 0);
	}
}
