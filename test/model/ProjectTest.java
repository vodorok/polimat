package model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProjectTest {

	private ProjectCollection pc;
	private Project proj_1;
	private RawMaterialType prof_1;
	private RawMaterialType prof_2;
	
	private Need need_1;

	@Before
	public void setUp() throws Exception {
		pc = ProjectCollection.GetInstace();
		prof_1 = new RawMaterialType("TEST 40x20x2 Zártszelvény", 1);
		prof_2 = new RawMaterialType("TEST 20x20x2 Zártszelvény", 1);
		
		proj_1 = new Project(0, "TEST Projekt 1");
		
		need_1 = new Need(prof_1, 700.0);
	} 
	
	@Test
	public void testProjectAddition() {
		pc.addProject(proj_1);
		Assert.assertEquals(pc.getAllProject().contains(proj_1), true);
	}

	@Test
	public void testNeedAddition() {
		proj_1.AddNeed(need_1);
		Assert.assertEquals(proj_1.getNeedList().contains(need_1), true);
	}
	
	@Test
	public void testNeedEdit() {
		proj_1.AddNeed(need_1);
		proj_1.EditNeed(need_1, prof_2, 500.0);
		Assert.assertEquals(proj_1.getNeedList().get(proj_1.getNeedList().indexOf(need_1)).getLength(), 500, 0);
		Assert.assertEquals(proj_1.getNeedList().get(proj_1.getNeedList().indexOf(need_1)).getProfile(), prof_2);
	}
	
}
