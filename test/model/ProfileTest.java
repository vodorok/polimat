package model;

import org.junit.Assert;
import org.junit.Test;

public class ProfileTest {

	@Test
	public void testNewProfile() {
		RawMaterialType prof = new RawMaterialType("20x20x2 Zártszelvény", 10);
		Assert.assertEquals("20x20x2 Zártszelvény", prof.getType());
	}
	
	@Test
	public void testEditProfile() {
		RawMaterialType prof = new RawMaterialType("20x20x2 Zártszelvény", 10);
		prof.setType("40x40x3 Zártszelvény");
		Assert.assertEquals("40x40x3 Zártszelvény", prof.getType());
	}
}
