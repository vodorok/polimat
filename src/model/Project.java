package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
 * Class for representing one Project.
 * The project has an ID and a name which is displayed on the project view / project list.
 * A project is also responsible for loading itself.
 * This class may needs heavy expansion depending on customer requests.
 */
@XmlRootElement(name = "Project")
public class Project {
	private StringProperty name;
	private ProjectTreeItem rootNode;
	private List<Need> needList;
	
	public Project() {
		this.name = new SimpleStringProperty();
		rootNode = new ProjectTreeItem(new Component(this, "", null), 0);
		needList = new ArrayList<Need>();
	}
	
	public Project(long id, String projectName) {
		this.name = new SimpleStringProperty(projectName);
		rootNode = new ProjectTreeItem(new Component(this, projectName, null), 0);
		needList = new ArrayList<Need>();
	}
	
	/**
	 * Builds the model representation of the entire project structure from the prepeared csv.
	 * The used ProjectTreeItem is a subclass of TreeItem, with an added level indicator.
	 * 
	 * @param fileName the input filename
	 */
	public void buildProjectTree(String fileName) {
		ProjectTreeItem current, newItem;
		current = rootNode;
		FileReader fr;
		try {
			fr = new FileReader("aufgel�ste St�li.csv");
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(fr);
			String line = "";
			while ((line = br.readLine()) != null) {
				String[] data = line.split(";");
				int level = Integer.parseInt(data[1]);
				
				newItem = new ProjectTreeItem(new Component(this, data[7] + " "+ data[5], data[3]), Integer.parseInt(data[1]));
				if(level == current.getLevel()) {
					//if we are on the same level, we add to our parent
					current.getParent().getChildren().add(newItem);
					// TODO Implement leaf component
					//current.getParent().getValue().setLeaf;
					current = newItem;
				} else if (level == current.getLevel() + 1) {
					//if we are on the level below, we add to ourself
					current.getChildren().add(newItem);
					//TODO
					//current.getValue().setLeaf;
					current = newItem;
					//buildProjectTree(list, current, idx)
				} else if (level < current.getLevel()) {
					//if we are on the level upper, we search for a level that equals for ours
					while(current.getLevel() != level) {
						current = (ProjectTreeItem) current.getParent();
					}
					current.getParent().getChildren().add(newItem);
					// TODO Implement leaf component
					//current.getParent().getValue().setLeaf;
					current = newItem;
				}
			}
		} catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

	public final StringProperty nameProperty() {
		return this.name;
	}
	
	@XmlAttribute(name = "name")
	public final String getName() {
		return this.nameProperty().get();
	}
	

	public final void setName(final String name) {
		this.nameProperty().set(name);
	}

	public ProjectTreeItem getRootNode() {
		return rootNode;
	}
	
	@XmlElement(name = "need")
	public List<Need> getNeedList() {
		return needList;
	}
	
	public void AddNeed(Need n) {
		needList.add(n);
	}
	
	/**
	 * For editing the need propeperites.
	 * @param n The neew we want to change
	 * @param p The new Profile
	 * @param l The new length
	 */
	public void EditNeed(Need n, RawMaterialType p, Double l) {
		if(needList.contains(n)) {
			needList.get(needList.indexOf(n)).setProfile(p);
			needList.get(needList.indexOf(n)).setLength(l);
		}
	}
}
