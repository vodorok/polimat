package model;

import javax.xml.bind.annotation.XmlElement;

/**
 * Class for representing the need of required raw materials of the projects.
 */
public class Need {
	private RawMaterialType profile;
	private Double length;
	
	public Need() {
	}
	
	public Need(RawMaterialType p, Double l) {
		setProfile(p);
		setLength(l);
	}
	
	public RawMaterialType getProfile() {
		return profile;
	}

	public void setProfile(RawMaterialType profile) {
		this.profile = profile;
	}
	@XmlElement(name = "length")
	public Double getLength() {
		return length;
	}

	public void setLength(Double length) {
		this.length = length;
	}
}
