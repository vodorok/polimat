package model;

/**
 * Class for representing the material of one Raw Material
 */
public class Material {
	private String name;
	private double density;
	
	public Material(String name, double density) {
		this.setName(name);
		this.density = density;
	}

	public double GetDensity() {
		return density;
	}

	public void SetDensity(double density) {
		this.density = density;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
