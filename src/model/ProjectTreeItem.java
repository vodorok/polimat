package model;

import javafx.scene.control.TreeItem;

/*
 * Represents one graphical treeitem in ProjectView
 */
public class ProjectTreeItem extends TreeItem<Component>{
	
	/**
	 * This is the extra thing in this class.
	 * Becuase of the input data, which specifies a tree order with levels
	 * the implementation had to be expanded with a level represntation.
	 */
	private int level;
	
	public ProjectTreeItem(Component c, int level) {
		super(c); 
		this.level = level; 
	}
	
	public void AddComponent(Component c) {
		this.getChildren().add(new TreeItem<Component>(c));
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
}
