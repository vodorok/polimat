package model.xmlwrappers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import model.RawMaterial;

/*
 * Hashmap adapter for xml saving.
 * JXDB doesn't know how to parse custom maps.
 * This implementation is for <ListOfEntry, Map<UUID, ArrayList<RawMaterial>>>
 */
public class HashMapAdapter extends XmlAdapter<ListOfEntry, Map<UUID, ArrayList<RawMaterial>>> {

	@Override
	public Map<UUID, ArrayList<RawMaterial>> unmarshal(ListOfEntry v) throws Exception {

		Map<UUID, ArrayList<RawMaterial>> map = new HashMap<UUID, ArrayList<RawMaterial>>();
        for(Entry entry : v.getList() ) {
            map.put(entry.getKey(), new ArrayList<RawMaterial>(entry.getList()));
        }
        return map;
	}

	@Override
	public ListOfEntry marshal(Map<UUID, ArrayList<RawMaterial>> map) throws Exception {
		ListOfEntry loe = new ListOfEntry();
        for(Map.Entry<UUID, ArrayList<RawMaterial>> mapEntry : map.entrySet()) {
            Entry entry = new Entry();
            entry.setKey( mapEntry.getKey() );
            entry.getList().addAll( mapEntry.getValue() );
            loe.getList().add(entry);
        }
        return loe;
	}
}
