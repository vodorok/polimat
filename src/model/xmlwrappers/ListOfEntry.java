package model.xmlwrappers;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/*
 * This class represents One complete Collection of key-value pair of profile ID 
 * and the list of it's correspong rawmaterial count.
 */
@XmlAccessorType(XmlAccessType.FIELD) 
public class ListOfEntry {
	@XmlElement(name = "type")
    private List<Entry> list;

	public ListOfEntry() {
		list = new ArrayList<Entry>();
	}
    
    public List<Entry> getList() {
        return list;
    }
}
