package model.xmlwrappers;

import java.util.ArrayList;
import java.util.UUID;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import model.RawMaterial;

/*
 * Helper class for XML loading.
 * This is one entry in the unmarshalled Raw Material data XML
 */
@XmlAccessorType(XmlAccessType.NONE) 
public class Entry {
    
    private UUID key;
    
    private ArrayList<RawMaterial> list = new ArrayList<RawMaterial>();
    @XmlElement(name = "UUID")
    public UUID getKey(){
    	return key;
    }
    
    public void setKey( UUID value ){
    	key = value;
    }
    @XmlElement(name = "raw_material")
    public ArrayList<RawMaterial> getList(){ return list; }
}
