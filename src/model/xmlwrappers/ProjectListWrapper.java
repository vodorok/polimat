package model.xmlwrappers;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import model.Project;

/*
 * Wrapper class for project list in ProfileListCollection.
 */
@XmlRootElement(name = "projects")
public class ProjectListWrapper {
    private ArrayList<Project> projects;

    public ProjectListWrapper() {
    	projects = new ArrayList<Project>();
    }
    
    @XmlElement(name = "project")
    public ArrayList<Project> getProjects() {
        return projects;
    }

    public void setProjects(ArrayList<Project> projects) {
        this.projects = projects;
    }
}
