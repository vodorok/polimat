package model.xmlwrappers;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import model.RawMaterial;

/*
 * Wrapper class for stock map list in Stock.
 * This is the first step of saving, and last of loading.
 * Needed by JXDB
 */
@XmlRootElement(name = "stock")
public class StockWrapper {
	
	private HashMap<UUID, ArrayList<RawMaterial>> stocklist;
	
	@XmlElement
	@XmlJavaTypeAdapter(HashMapAdapter.class)
    public HashMap<UUID, ArrayList<RawMaterial>> getStock() {
        return stocklist;
    }

    public void setStock(HashMap<UUID, ArrayList<RawMaterial>> stock) {
        this.stocklist = stock;
    }
}
