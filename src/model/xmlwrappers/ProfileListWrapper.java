package model.xmlwrappers;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import model.RawMaterialType;


//http://code.makery.ch/library/javafx-8-tutorial/part5/
/*
 * Wrapper class for profile list in Stock.
 */
@XmlRootElement(name = "types")
public class ProfileListWrapper {
    private ArrayList<RawMaterialType> profiles;

    public ProfileListWrapper() {
    	profiles = new ArrayList<RawMaterialType>();
    }
    
    @XmlElement(name = "profile")
    public ArrayList<RawMaterialType> getProfiles() {
        return profiles;
    }

    public void setProfiles(ArrayList<RawMaterialType> profiles) {
        this.profiles = profiles;
    }
}
