package model;
import java.util.Map;
import java.util.Set;

public class RawMaterialTypeTemplate {
	private String name;
	private Set<String> stats;
	
	public RawMaterialTypeTemplate(String n, Set<String> s) {
		setName(n);
		setStats(s);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getStats() {
		return stats;
	}

	public void setStats(Set<String> stats) {
		this.stats = stats;
	}
	
	public String toString() {
		String str = name + " stats:" + stats.toString();
		return str;
	}
}
