package model;

import java.util.Set;
import java.util.UUID;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Class for representing one distinct type raw material type.
 */
@XmlRootElement(name = "Profile")
public class RawMaterialType {
	private RawMaterialTypeTemplate tmp;
	private Set<String> stats;
	
	private UUID id;
	private StringProperty type; 

	// The combined length of it's raw materials.
	private DoubleProperty allLengths;

	public RawMaterialType(){
		this.type = new SimpleStringProperty();
		this.allLengths = new SimpleDoubleProperty();
	}
	public RawMaterialType(String type, double area) {
		this.id = UUID.randomUUID();
		this.type = new SimpleStringProperty(type);
		this.allLengths = new SimpleDoubleProperty();
	}
	
	@XmlElement
	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}
	
	@XmlElement
	public String getType() {
		return this.type.get();
	}
	
	public void setType(String type) {
		this.type.set(type);
	}

	public final DoubleProperty allLengthsProperty() {
		return this.allLengths;
	}
	
	public final StringProperty typeProperty() {
		return this.type;
	}

	public final double getAllLengths() {
		return this.allLengthsProperty().get();
	}
	

	public final void setAllLengths(final double allLengths) {
		this.allLengthsProperty().set(allLengths);
	}
}
