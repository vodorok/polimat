package model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.xmlwrappers.ProjectListWrapper;

/*
 * Class that manages the full ist of projects.
 * Responsible for maintaning the actual list and it't wrapper observable counterpart.
 * The class loads itself during initialization.
 */
@XmlRootElement(name = "Projects")
@XmlAccessorType (XmlAccessType.NONE)
public class ProjectCollection {
	@XmlElement(name = "Project")
	private List<Project> projectList;
	public ObservableList<Project> observableProjectList;
	
	public List<Need> needList = new ArrayList<Need>();
	
	private static ProjectCollection projectCollectionInstance = new ProjectCollection();
	public static ProjectCollection GetInstace(){
		return projectCollectionInstance;
	}
	
	private ProjectCollection() {
		projectList = new ArrayList<Project>();
		observableProjectList = FXCollections.observableArrayList(projectList);
		//TODO remove these
		addProject(new Project(0, "Main Track"));
		//observableProjectList.get(0).buildProjectTree("root");
		LoadProjectData();
	}
	
	/**
	 * returns an observable list. this can be connected in a view straight away.
	 * @return the observable list list.
	 */
	public ObservableList<Project> getAllProject(){
		return observableProjectList;
	}
	
	/**
	 * For XML save/load
	 * @return 
	 */
	public ArrayList<Project> getAllProjectXML(){
		return (ArrayList<Project>) projectList;
	}
	
	/**
	 * Adds one Project to the Project Collection
	 * @param p The project to be added.
	 */
	public void addProject(Project p) {
		projectList.add(p);
		observableProjectList.add(p);
	}
	
	/**
	 * For XML save/load
	 * @param p
	 */
	private void addProjects(ArrayList<Project> p) {
		projectList.addAll(p);
		observableProjectList.addAll(p);
	}
	
	/**
	 * Removes the paramter specified project from the project collection.
	 * @param p the project to remove
	 */
	public void removeProject(Project p) {
		projectList.remove(p);
		observableProjectList.remove(p);
	}
	
	/**
	 * cleas both project lists
	 */
	private void clearProjectLists() {
		projectList.clear();
		observableProjectList.clear();
	}
	
	/**
	 * Class for handilng databese save to xml with JXDB.
	 * Saving is handled on exit.
	 * ProjectData.xml is the oustput file.
	 */
	public void SaveProjectData() {
		try {
			JAXBContext context = JAXBContext.newInstance(ProjectListWrapper.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			ProjectListWrapper wrapper = new ProjectListWrapper();
	        wrapper.setProjects(ProjectCollection.GetInstace().getAllProjectXML());
			m.marshal(wrapper, new File("ProjectData.xml"));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Class for handling database load from xml with JXDB
	 * Loading is handled in the class constructor.
	 */
	public void LoadProjectData() {
		try {
	        JAXBContext context = JAXBContext.newInstance(ProjectListWrapper.class);
	        Unmarshaller u = context.createUnmarshaller();
	        ProjectListWrapper wrapper = (ProjectListWrapper) u.unmarshal(new File("ProjectData.xml"));
	        clearProjectLists();
	        addProjects(wrapper.getProjects());
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	public void sortNeeds() {		
		// Get all need from all projects.
		
		// Sort them by types then lengths descending.
		
		// Apply the cutting stock problem solving to each of them.
		
		// Maybe display a window of the applied sorts.
	}
}
