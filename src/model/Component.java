package model;

/**
 * Class for representing one node in project tree.
 */
public class Component {
	private String id;
	private String planName;
	private Project project;
	
	public Component(Project pr, String id, String pn) {
		this.project = pr;
		this.id = id;
		this.planName = pn;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
}
