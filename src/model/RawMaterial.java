package model;

import java.util.ArrayList;
import java.util.UUID;

/*
 * Respresents one actual stock item of a certaion Profile type.
 * The Profile can be in three state: 
 * - fre - ethere is no reserve on the resource
 * - reserved + resource that isn't on strock
 * - needed - resource that isn't on strock
 * and the most important member the length.
 */
public class RawMaterial {
	private double length;
	private RawMaterialType type;
	private Material material;
	private ArrayList<Need> needList = new ArrayList<Need>();
	private UUID id;
	
	public RawMaterial() {
	}
	
	public RawMaterial(RawMaterialType t, 
			Material m, 
			double l) {
		this.type = t;
		this.material = m;
		this.length = l;
		this.id = UUID.randomUUID();
	}
	
	public double GetWeight() {
		// TODO
		return 0;
	}
	
	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}
	
	/**
	 * this enum contains the available states of the RawMaterial.
	 * @author vodor
	 *
	 */
	public enum State{
		FREE, 		// there is no reserve on the resource
		NEEDED,		// resource that isn't on strock
		RESERVED,	// there is a reserve on the resource
	}

	/*public State getState() {
		return state;
	}

	// TODO this could be set reserved/free/etc...
	public void setState(State state) {
		this.state = state;
	}*/

	public UUID getId() {
		return id;
	}
	

	public void setId(final UUID id) {
		this.id = id;
	}
	
	/*
	 * Assigns a Need to this concrete RawMaterial.
	 */
	public void addNeed(Need n) {
		//TODO before this, check if the incoming need fits the remaining free piece.
		needList.add(n);
	}
	
	/*
	 * Clears the List of needs of this raw material.
	 */
	public void cleadNeedList() {
		needList.clear();
	}
}
