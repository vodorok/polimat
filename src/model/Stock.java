package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.transform.Templates;

import com.google.gson.Gson;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import model.xmlwrappers.ProfileListWrapper;
import model.xmlwrappers.StockWrapper;

/**
 * DAO pattern.
 * This Class represents the available raw materials on stock.
 * Also maintains the available types (Profiles) of that. There are actual list
 * and map and their wrapper observable counterparts.
 * The Class is a singleton, this is the reason of the lot of wrapper class
 * becuase JXDB needed a custom intializer of this class.
 * The class loads itself during initialization.
 */
@XmlRootElement(name = "Stock")
@XmlAccessorType (XmlAccessType.NONE)
public class Stock 
{
	private HashMap<UUID, ObservableList<RawMaterial>> stock;
	private List<RawMaterialType> profileList;
	
	private ObservableMap<UUID, ObservableList<RawMaterial>> observableStock;
	private ObservableList<RawMaterialType> observableProfileList;
	
	//TODO make this private
	public List<RawMaterialTypeTemplate> templatelist = new ArrayList<>();
	
	private static Stock stockInstance = new Stock();
	public static Stock GetInstace(){
		return stockInstance;
	}
	
	private Stock() {
		stock = new HashMap<UUID, ObservableList<RawMaterial>>();
		observableStock = FXCollections.observableMap(stock);
		profileList = new ArrayList<RawMaterialType>();
		observableProfileList = FXCollections.observableArrayList(profileList);
		LoadStockData();
	}
	
	/** Returns a list containing every length by the specified profile
	 * @param profile
	 * @return
	 */
	public ObservableList<RawMaterial> GetProfileRaws(RawMaterialType profile) {
		if(observableStock.containsKey(profile.getId()))
			return observableStock.get(profile.getId());
		//TODO
		//else throw;
		return null;
	}
	
	/**
	 * Adds a new RawMaterial to the specified profile type.
	 * @param profile
	 * @param raw
	 */
	public void AddRaw(RawMaterialType profile, RawMaterial raw) {
		ObservableList<RawMaterial> rList = GetProfileRaws(profile);
		rList.add(raw);
		
		Double combinedLenth = 0.0;
		for (RawMaterial rawMaterial : rList) {
			combinedLenth += rawMaterial.getLength();
		}
		
		observableProfileList.get(observableProfileList.indexOf(profile)).setAllLengths(combinedLenth);
	}

	public void EditRaw(RawMaterialType profile, RawMaterial raw, Double l) {
		ObservableList<RawMaterial> rList = GetProfileRaws(profile);
		if(rList.contains(raw)) {
			rList.get(rList.indexOf(raw)).setLength(l);;
		}
		
		Double combinedLenth = 0.0;
		for (RawMaterial rawMaterial : rList) {
			combinedLenth += rawMaterial.getLength();
		}
		
		observableProfileList.get(observableProfileList.indexOf(profile)).setAllLengths(combinedLenth);
	}
	
	/**
	 * Adds One profile to the profile list.
	 * @param profile the profile to be added.
	 */
	public void AddProfile(RawMaterialType profile) {
		this.observableStock.put(profile.getId(), FXCollections.observableArrayList(new ArrayList<RawMaterial>()));
		this.observableProfileList.add(profile);
		this.profileList.add(profile);
	}
	
	/**
	 * For XML save/load
	 * @param profiles
	 */
	private void AddProfiles(ArrayList<RawMaterialType> profiles) {
		this.observableProfileList.addAll(profiles);
		this.profileList.addAll(profiles);
	}
	
	/**
	 * For XML save/load
	 * @param loadedStock
	 */
	private void AddRaws(HashMap<UUID, ArrayList<RawMaterial>> loadedStock) {
		for (Entry<UUID, ArrayList<RawMaterial>> entry : loadedStock.entrySet())
		{
		    stock.put(entry.getKey(), FXCollections.observableArrayList(entry.getValue()));
		    observableStock.put(entry.getKey(), FXCollections.observableArrayList(entry.getValue()));
		}
	}
	
	/**
	 * this method changes the properities of the given profile
	 * but not by replaciong it, but changing all of it's properities.
	 * UUID stays the same.
	 * @param profile the profile to be edited.
	 * @param edited the editing profile
	 */
	public void EditProfile(RawMaterialType profile,RawMaterialType edited) {
		//check if profileList contains profile
		if(observableProfileList.contains(profile)) {
			RawMaterialType old = observableProfileList.get(observableProfileList.indexOf(profile));
			old.setType(edited.getType());
			// TODO
			//old.SetArea(edited.GetArea());
		}
	}
	
	
	/**
	 * removes one profile from the profile list.
	 * @param profile the profile to be removed.
	 */
	public void RemoveProfile(RawMaterialType profile) {
		this.profileList.remove(profile);
		this.stock.remove(profile.getId());
		
		// this should be sufficent but it isn't
		this.observableStock.remove(profile.getId());
		this.observableProfileList.remove(profile);
	}
	
	/**
	 * returns all profiles from the stock
	 * @return
	 */
	public ObservableList<RawMaterialType> GetAllProfile(){
		return observableProfileList;
	}
	
	/**
	 * For XNL save/load
	 * @return
	 */
	private ArrayList<RawMaterialType> getProfileList(){
		return (ArrayList<RawMaterialType>) profileList;
	}
	
	/**
	 * For XNL save/load
	 * @return
	 */
	private HashMap<UUID, ArrayList<RawMaterial>> getStock(){
		HashMap<UUID, ArrayList<RawMaterial>> map = new HashMap<UUID, ArrayList<RawMaterial>>();
		for (Entry<UUID, ObservableList<RawMaterial>> entry : stock.entrySet())
		{
		    map.put(entry.getKey(), new ArrayList<RawMaterial>(entry.getValue()));
		}
		return map;
	}
	
	private void clearProfileLists(){
		profileList.clear();
		observableProfileList.clear();
	}
	
	/**
	 * Class for handling database save to xml with JXDB
	 * Saving is handled on exit. There are two xmls for the two datatypes.
	 * StockData.xml for the project list.
	 * StockData3.xml for the stock.
	 */
	public void SaveStockData() {
		try {
			JAXBContext context = JAXBContext.newInstance(ProfileListWrapper.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        ProfileListWrapper wrapper = new ProfileListWrapper();
	        wrapper.setProfiles(Stock.GetInstace().getProfileList());
			m.marshal(wrapper, new File("StockData.xml"));
			
			context = JAXBContext.newInstance(StockWrapper.class);
			m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        StockWrapper stockWrapper = new StockWrapper();
	        stockWrapper.setStock(Stock.GetInstace().getStock());
			m.marshal(stockWrapper, new File("StockData2.xml"));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Class for handling database load from xml with JXDB
	 * Loading is handled in the class constructor.
	 */
	public void LoadStockData() {
		try {
	        JAXBContext context = JAXBContext.newInstance(ProfileListWrapper.class);
	        Unmarshaller u = context.createUnmarshaller();
	        ProfileListWrapper wrapper = (ProfileListWrapper) u.unmarshal(new File("StockData.xml"));
	        clearProfileLists();
	        AddProfiles(wrapper.getProfiles());
	        
	        context = JAXBContext.newInstance(StockWrapper.class);
	        u = context.createUnmarshaller();
	        StockWrapper stockWrapper = (StockWrapper) u.unmarshal(new File("StockData2.xml"));
	        AddRaws(stockWrapper.getStock());
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			BufferedReader br = new BufferedReader(
			           new InputStreamReader(new FileInputStream("typeTemplates.txt"), "UTF-8"));
			StringBuilder sb = new StringBuilder();
			
			
			String json = new String();
			String line = new String();
			while ((line = br.readLine()) != null) {
		        sb.append(line);
		    }
			br.close();
			
			json = sb.toString();

			
			Gson gson = new Gson();
			RawMaterialTypeTemplate[] arr = gson.fromJson(json, RawMaterialTypeTemplate[].class);
			templatelist = Arrays.asList(arr); 
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
