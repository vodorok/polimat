package view;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.Material;
import model.RawMaterialType;
import model.RawMaterial;
import model.Stock;

/*
 * This class is the controller of the stock part of the application.
 * It"s associated model is the Stock singleton class, and the view part of the MVC is
 * handled in StockView.fxml.
 */
public class StockViewController implements Initializable{
	
	private Stage primaryStage;
	
	private Stock stock = Stock.GetInstace();
	
	@FXML private TableView<RawMaterial> elementTable;	
	@FXML private TableColumn<RawMaterial, Double> lengthColumn;
	@FXML private ListView<RawMaterialType> profileListView;
	
	@FXML private Button addProfileButton;
	
	@FXML private Button addRawButton;
	@FXML private AnchorPane rawPane;
	@FXML private TextField rawLengthText;
	@FXML private Button confirmRawButton;
	@FXML private Button closeRWPBtn;
	private boolean editRawState = false;
	
	private RawMaterialType editedProfile;
	private RawMaterial editedRaw;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		// Notice the length getter- setters in Raw Material.
		PropertyValueFactory<RawMaterial, Double> lengthPvf = new PropertyValueFactory<>("length");
		lengthColumn.setCellValueFactory(lengthPvf);
		
		profileListView.setItems(stock.GetAllProfile());
		profileListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<RawMaterialType>() {

			@Override
			public void changed(ObservableValue<? extends RawMaterialType> observable, RawMaterialType oldVal, RawMaterialType newVal) {
				elementTable.getItems().clear();
				elementTable.getItems().addAll(stock.GetProfileRaws(newVal));
				
				
			}
		});
		
		profileListView.setCellFactory(new Callback<ListView<RawMaterialType>, javafx.scene.control.ListCell<RawMaterialType>>()
        {
            @Override
            public ListCell<RawMaterialType> call(ListView<RawMaterialType> profileListView)
            {
                return new ProfileListCell(StockViewController.this);
            }
        });
		
		lengthColumn.setCellFactory(new Callback<TableColumn<RawMaterial,Double>, TableCell<RawMaterial,Double>>() {
			
			@Override
			public TableCell<RawMaterial, Double> call(TableColumn<RawMaterial, Double> param) {
				return new TableCell<RawMaterial, Double>(){
					
					@Override
					protected void updateItem(Double item, boolean empty) {
						super.updateItem(item, empty);
						if (item != null) {
							// TODO Define this in CSS
							setStyle("-fx-background-color: rgba(150, 255, 150, 1)");
							setText(Double.toString(item));
						} else {
							setStyle(null);
							setText(null);
						}
					}
				};
			}
		});
		HideEditRawActionListener(null);
	}
	
	@FXML protected void ShowEditProfileActionListener() {
		showNewRawTypeDialog(null);
	}
	
	public boolean showNewRawTypeDialog(RawMaterialType r) {
	    try {
	        // Load the fxml file and create a new stage for the popup dialog.
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(RootController.class.getResource("RawMaterialDialog.fxml"));
	        BorderPane page = (BorderPane) loader.load();

	        // Create the dialog Stage.
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Edit Person");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);

	        // Set the person into the controller.
	        RawMaterialDialogController controller = loader.getController();
	        controller.setDialogStage(dialogStage);
	        if (r != null)
	        	controller.setRawMaterialType(r);

	        // Show the dialog and wait until the user closes it
	        dialogStage.showAndWait();
	        return controller.isOkClicked();
	    } catch (IOException e) {
	        e.printStackTrace();
	        return false;
	    }
	}

	@FXML
	private void ShowEditRawActionListener(ActionEvent event) {
		if(editRawState) {
			rawPane.setVisible(true);
			rawPane.setManaged(true);
			editRawState = false;
			editedRaw = elementTable.getSelectionModel().getSelectedItem();
			if(editedRaw != null) {
				rawLengthText.setText(Double.toString(editedRaw.getLength()));
			}
		}
	}
	
	@FXML
	private void HideEditRawActionListener(ActionEvent event) {
		if(!editRawState) {
			rawPane.setVisible(false);
			rawPane.setManaged(false);
			editRawState = true;
			ClearRawPanel();
		}
	}
	
	@FXML
	private void addRawActionListener(ActionEvent event) {
		if(getSelectedProfile() != null & rawLengthText.getText() != null) {
			if(editedRaw != null) {
				stock.EditRaw(getSelectedProfile(), editedRaw, Double.parseDouble(rawLengthText.getText()));
			} else {
				stock.AddRaw(getSelectedProfile(),
						new RawMaterial(getSelectedProfile(),
						new Material("vas", 1.0), 
						Double.parseDouble(rawLengthText.getText())));
			}
			elementTable.getItems().clear();
			elementTable.getItems().addAll(stock.GetProfileRaws(getSelectedProfile()));
		}
		HideEditRawActionListener(null);
	}	

	public void ClearRawPanel() {
		editedRaw = null;
		rawLengthText.setText(null);
	}
	
	private RawMaterialType getSelectedProfile() {
		return profileListView.getSelectionModel().getSelectedItem();
	}
	
	public void setPrimaryStage(Stage s) {
		this.primaryStage = s;
	}
}
