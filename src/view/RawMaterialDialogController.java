package view;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;
import model.RawMaterialType;
import model.RawMaterialTypeTemplate;
import model.Stock;

public class RawMaterialDialogController implements Initializable {
	
	@FXML private ComboBox<RawMaterialTypeTemplate> templateCbox;
	@FXML private GridPane gPane;
	@FXML private Button okButton;
	@FXML private Button cancelButton;
	
	private Stage dialogStage;
	private boolean okClicked = false;
	
	Stock stock = Stock.GetInstace();
	RawMaterialType rmt = null;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		templateCbox.getItems().addAll(stock.templatelist);
		
		//https://stackoverflow.com/questions/41201043/javafx-combobox-using-object-property
		Callback<ListView<RawMaterialTypeTemplate>, ListCell<RawMaterialTypeTemplate>> 
								factory = lv -> new ListCell<RawMaterialTypeTemplate>() {

		    @Override
		    protected void updateItem(RawMaterialTypeTemplate item, boolean empty) {
		        super.updateItem(item, empty);
		        setText(empty ? "" : item.getName());
		    }

		};
		templateCbox.setCellFactory(factory);
		templateCbox.setButtonCell(factory.call(null));
		
		// ChangeListener for the Type selector
		templateCbox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<RawMaterialTypeTemplate>() {

			@Override
			public void changed(ObservableValue<? extends RawMaterialTypeTemplate> observable, RawMaterialTypeTemplate oldValue, RawMaterialTypeTemplate newValue) {
				gPane.getChildren().clear();
				ArrayList<String> list = new ArrayList<String>(templateCbox.getSelectionModel().getSelectedItem().getStats());
				for(int i = 0; i < list.size(); i++) {
					gPane.add(new Text(list.get(i)), 0, i);
					if (rmt != null)
						gPane.add(new TextField("100"), 1, i);
					else
						gPane.add(new TextField(), 1, i);
				}
			}
		});
	}
	
	public void setRawMaterialType(RawMaterialType r) {
		rmt = r;
	}

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }
    
    @FXML
    private void okClicked() {
    	okClicked = true;
    	dialogStage.close();
    }
    
    @FXML
    private void cancelClicked() {
    	dialogStage.close();
    }
    
    public boolean isOkClicked() {
        return okClicked;
    }
}
