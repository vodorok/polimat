package view;

import java.io.IOException;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import model.RawMaterialType;
import model.Stock;

/*
 * This is a custom list cell for displaying object templated listview.
 * the class Initializes the ListCellView controller from ProfileListCell.fxml
 */
public class ProfileListCell extends ListCell<RawMaterialType> {
	private Node graphic;
	private ListCellView controller;
	
	public ProfileListCell(StockViewController parent)
	{
		try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ProfileListCell.fxml"));
            graphic = loader.load();
            controller = loader.getController();
        } catch (IOException exc) {
            throw new RuntimeException(exc);
        }
        //controller.getDelButton().setOnAction(event -> getListView().getItems().remove(getItem()));
		controller.getDelButton().setOnAction(event -> Stock.GetInstace().RemoveProfile(getItem()));
        controller.getEditButton().setOnAction(event -> { 
        	// TODO az �j dialogot is ki kell t�lteni
        	parent.showNewRawTypeDialog(getItem());
        });
	}	
	
	@Override
    public void updateItem(RawMaterialType profile, boolean empty) {
        super.updateItem(profile, empty);
        textProperty().unbind();
        if (profile != null) {
        	this.hoverProperty().addListener( (obsVal, oldVal, newVal) -> {
    		//this.selectedProperty().addListener( (obsVal, oldVal, newVal) -> {
                if(newVal){
                    //selected
                	controller.ShowButtons();;
                } else {
                	controller.HideButtons();
                }
            });
    		controller.getOverallLgtText().textProperty().bind(Bindings.convert(profile.allLengthsProperty()));
    		controller.getTypeText().textProperty().bind(profile.typeProperty());
    		setGraphic(graphic);
        } else {
        	setText(null);
            setGraphic(null);
        }
	}
}
