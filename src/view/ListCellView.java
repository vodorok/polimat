package view;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;

/*
 * This class represents one entry in the profile list of the stock view.
 * 
 */
public class ListCellView {
	@FXML private Text type;
	@FXML private Text overallLgt;
	@FXML private Button edit;
	@FXML private Button delete;
	
    public void setType(String name) {
    	type.setText(name);
    }

    public void setOverallLgt(String status) {
    	overallLgt.setText(status);
    }
    
    public Button getEditButton() {
    	return edit;
    }
    
    public Button getDelButton() {
    	return delete;
    }
    
    public Text getTypeText() {
    	return this.type;
    }
    
    public Text getOverallLgtText() {
    	return this.overallLgt;
    }

    public void HideButtons() {
    	edit.setVisible(false);
    	delete.setVisible(false);
    }
    
    public void ShowButtons() {
    	edit.setVisible(true);
    	delete.setVisible(true);
    }
}
