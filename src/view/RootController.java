package view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import transition.HorizontaParentResizeTransition;

/*
 * This class represents the base of the application. The sidebar is tied to this class,
 * and acts as a tunnel between the project and stock view.
 */
public class RootController implements Initializable {
	
	@FXML private VBox sideBar;
	
	@FXML private Button menuButton;
	@FXML private Button stockButton;
	@FXML private Button projectButton;
	
	private boolean minimized = true;
	
	private HBox stockView;
	private HBox projectView;
	private BorderPane rootLayout;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Change Titles of Buttons
		menuButton.setOnMouseClicked(new EventHandler<Event>() {

			@Override
			public void handle(Event event) {
				HorizontaParentResizeTransition hrt;
				if(minimized) {
					hrt = new HorizontaParentResizeTransition(sideBar, Duration.millis(50), 260);
					stockButton.setText("  Készlet");
					stockButton.setAlignment(Pos.BASELINE_LEFT);
					projectButton.setText("  Projektek");
					stockButton.setAlignment(Pos.BASELINE_LEFT);
					projectButton.setAlignment(Pos.BASELINE_LEFT);
					menuButton.setAlignment(Pos.BASELINE_LEFT);
					minimized = false;
				} else {
					hrt = new HorizontaParentResizeTransition(sideBar, Duration.millis(50), 50);
					stockButton.setText("");
					stockButton.setAlignment(Pos.BASELINE_CENTER);
					projectButton.setText("");
					projectButton.setAlignment(Pos.BASELINE_CENTER);
					menuButton.setAlignment(Pos.BASELINE_CENTER);
					minimized = true;
				}
				hrt.play();
			}
		});
		
		projectButton.setOnMouseClicked(mouseEvent -> {
			showProjectView();
		});
		
		stockButton.setOnMouseClicked(mouseEvent -> {
			showStockView();
		});
	}
	
	public void setCenter(Node node) {
		rootLayout.setCenter(node);
	}
	
	public void showProjectView() {
		rootLayout.setCenter(projectView);
	}
	
	public void showStockView() {
		rootLayout.setCenter(stockView);
	}
	
    public void setStocView(Node n){
    	this. stockView = (HBox) n;
    }
    
    public void setProjectView(Node n) {
		this. projectView = (HBox) n;
	}
    
	public void setRootLayout(Node n) {
		this.rootLayout = (BorderPane) n;
	}
}
