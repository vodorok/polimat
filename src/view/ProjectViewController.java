package view;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.HostServices;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import model.Component;
import model.Need;
import model.RawMaterialType;
import model.Project;
import model.ProjectCollection;
import model.Stock;

/*
 * The controller for the entire Project view.
 * It's associated modell is the ProjectCollection singleton class.
 * and the view is handled in ProjectView.fxml
 * The vall is responsible for setting the data in the view defined controls,
 * and setting proper cellfactories where it's needed.
 * File opening mechanism is through HostServices.
 */
public class ProjectViewController implements Initializable{
	
	private ProjectCollection pc = ProjectCollection.GetInstace();
	
    private HostServices hostServices ;
	
	@FXML private ListView<Project> projects;
	@FXML private TreeView<Component> components;
	@FXML private ListView<Need> needList;
	
	@FXML private Button addNeedBtn;
	@FXML private VBox needPane;
	@FXML private ComboBox<RawMaterialType> profileTypeCBox;
	@FXML private TextField needLengthText;
	@FXML private Button confirmNeedButton;
	@FXML private Button closeNPBtn;
	
	private boolean editNeedState;
	private Need editedNeed;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		components.setShowRoot(false);
		projects.setItems(pc.getAllProject());
		profileTypeCBox.getItems().addAll(Stock.GetInstace().GetAllProfile());
		
		// TODO This should depend on selection.
		projects.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Project>() {

			@Override
			public void changed(ObservableValue<? extends Project> observable, Project oldVal, Project newVal) {
				projects.getSelectionModel().getSelectedItem().buildProjectTree("root");
				components.setRoot(projects.getSelectionModel().getSelectedItem().getRootNode());
				needList.getItems().clear();
				needList.getItems().addAll(projects.getSelectionModel().getSelectedItem().getNeedList());
			}
		});
		
		components.setCellFactory(new Callback<TreeView<Component>, TreeCell<Component>>() {
			
			// TODO extract the anonymus class
			@Override
			public TreeCell<Component> call(TreeView<Component> param) {
				TreeCell<Component> cell = new TreeCell<Component>() {
					@Override
					protected void updateItem(Component item, boolean empty) {
						super.updateItem(item, empty);
						if(item != null) {
							setText(item.getId());
						} else {
							setText(null);
						}
					}
				};
				return cell;
			}
		});
		
		projects.setCellFactory(new Callback<ListView<Project>, ListCell<Project>>() {
			
			@Override
			public ListCell<Project> call(ListView<Project> param) {
				ListCell<Project> cell = new ListCell<Project>() {
					@Override
					protected void updateItem(Project item, boolean empty) {
						super.updateItem(item, empty);
						if(item != null) {
							setText(item.getName());
						} else {
							setText(null);
						}
					}
				};
				return cell;
			}
		});
		
		needList.setCellFactory(new Callback<ListView<Need>, ListCell<Need>>() {
			
			@Override
			public ListCell<Need> call(ListView<Need> param) {
				ListCell<Need> cell = new ListCell<Need>() {
					@Override
					protected void updateItem(Need item, boolean empty) {
						super.updateItem(item, empty);
						if(item != null) {
							setText(item.getProfile().getType() + " - " + item.getLength() + " mm");
						} else {
							setText(null);
						}
					}
				};
				return cell;
			}
		});

		//https://stackoverflow.com/questions/41201043/javafx-combobox-using-object-property
		Callback<ListView<RawMaterialType>, ListCell<RawMaterialType>> factory = lv -> new ListCell<RawMaterialType>() {

		    @Override
		    protected void updateItem(RawMaterialType item, boolean empty) {
		        super.updateItem(item, empty);
		        setText(empty ? "" : item.getType());
		    }

		};
		profileTypeCBox.setCellFactory(factory);
		profileTypeCBox.setButtonCell(factory.call(null));
		
		HideEditNeedActionListener(null);
	}

    public HostServices getHostServices() {
        return hostServices ;
    }

    public void setHostServices(HostServices hostServices) {
        this.hostServices = hostServices;
    }
    
    private void openFile(String directory, String fileName) {
    	hostServices.showDocument(directory + "\\" + fileName + "_00.DXF");
    }
    
    @FXML
    private void mouseClick(MouseEvent mouseEvent) {
    	
    	String directory = "DXF_BA-170025-010";
    	if (mouseEvent.getClickCount() == 2) {
    		String fileName = components.getSelectionModel().getSelectedItem().getValue().getPlanName();
    		openFile(directory, fileName);
    	}
    }
	
	@FXML
	private void ShowEditNeedActionListener(ActionEvent event) {
		if(editNeedState) {
			needPane.setVisible(true);
			needPane.setManaged(true);
			editNeedState = false;
			editedNeed = needList.getSelectionModel().getSelectedItem();
			if(editedNeed != null) {
				//profileTypeCBox.getSelectionModel().select(editedNeed.getProfile());
				needLengthText.setText(Double.toString(editedNeed.getLength()));
			}
		}
	}
	
	@FXML
	private void HideEditNeedActionListener(ActionEvent event) {
		if(!editNeedState) {
			needPane.setVisible(false);
			needPane.setManaged(false);
			editNeedState = true;
			ClearNeedPanel();
		}
	}
	
	@FXML
	private void addNeedActionListener(ActionEvent event) {
		//TODO should depend on project
		Project selectedProj = projects.getSelectionModel().getSelectedItem();
		if(selectedProj != null & needLengthText.getText() != null) {
			if(editedNeed != null) {
				selectedProj.EditNeed(editedNeed, profileTypeCBox.getSelectionModel().getSelectedItem(), 
						Double.parseDouble(needLengthText.getText()));
			} else {
				selectedProj.AddNeed(new Need(profileTypeCBox.getSelectionModel().getSelectedItem(), 
						Double.parseDouble(needLengthText.getText())));
			}
			needList.getItems().clear();
			needList.getItems().addAll(selectedProj.getNeedList());
		}
		HideEditNeedActionListener(null);
	}	

	public void ClearNeedPanel() {
		editedNeed = null;
		needLengthText.setText(null);
		profileTypeCBox.getSelectionModel().selectFirst();
	}
}
