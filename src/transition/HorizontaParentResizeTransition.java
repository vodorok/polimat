package transition;

import javafx.animation.Transition;
import javafx.scene.layout.Region;
import javafx.util.Duration;

/*
 * Class that respresents one container resise animation.
 */
public class HorizontaParentResizeTransition extends Transition{
	Region region;
	double startingWidth;
	double endWidth;
	
	double height;
	
	public HorizontaParentResizeTransition(Region r, Duration d, double e) {
		setCycleDuration(d);
		this.region = r;
		this.startingWidth = region.getWidth();
		this.endWidth = e;
		
		this.height = r.getHeight();
	}

	@Override
	protected void interpolate(double frac) {
		region.setMinWidth(startingWidth + ((endWidth - startingWidth) * frac));
	}

}
