
import java.io.IOException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.ProjectCollection;
import model.RawMaterialType;
import model.Stock;
import view.RawMaterialDialogController;
import view.ProjectViewController;
import view.RootController;
import view.StockViewController;

public class MainApp extends Application {
	
	private Stage primaryStage;
	private BorderPane rootLayout;
	private HBox stockView;
	private HBox projectView;
	private RootController controller;
	private ProjectViewController pController;
	private StockViewController sController;
	
	/*
	 * (non-Javadoc)
	 * @see javafx.application.Application#start(javafx.stage.Stage)
	 */
	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
	    
		primaryStage.initStyle(StageStyle.DECORATED);		
		primaryStage.getProperties().put("hostServices", this.getHostServices());
		
		primaryStage.setTitle("Polimat");
		
		primaryStage.show();
		
		primaryStage.setOnCloseRequest(event -> {
			Stock.GetInstace().SaveStockData();
			ProjectCollection.GetInstace().SaveProjectData();
		});

		Platform.setImplicitExit(false);
		
		initRootLayout();
		 
		// should call these from InitRoot?
		initStockView();
		initProjectView();
		 
		controller.setCenter(stockView);
	}

	/*
	 * Initializes StockView
	 */
	private void initStockView() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/StockView.fxml"));
			stockView = (HBox) loader.load();
			stockView.setBackground(Background.EMPTY);
			controller.setStocView(stockView);
			sController = (StockViewController)loader.getController();
			sController.setPrimaryStage(primaryStage);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * Initalizes ProjectViev form ProjectView.fxml
	 */
	private void initProjectView() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/ProjectView.fxml"));
			projectView = (HBox) loader.load();
			projectView.setBackground(Background.EMPTY);
			controller.setProjectView(projectView);
			pController = (ProjectViewController)loader.getController();
			pController.setHostServices(getHostServices());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initalizes Rootlayout, from RootLayout.fxml. Sets the default window size to 1300x900.
	 */
	private void initRootLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();
			
			Scene scene = new Scene(rootLayout, 1300, 900);
			scene.setFill(Color.TRANSPARENT);
			primaryStage.setScene(scene);
			
			rootLayout.setBackground(Background.EMPTY);
			
			controller = (RootController)loader.getController();
			controller.setRootLayout(rootLayout);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Entry point for the program
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
